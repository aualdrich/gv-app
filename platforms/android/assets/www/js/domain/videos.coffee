window.load_videos_page = () ->
	 bind_videos()	


bind_videos = () ->
	template = $("#video_list_item_script").html()

	$.mobile.loading("show")
	
	episodes = localStorage.getItem("episodes")
	data = JSON.parse(episodes)

	html = Mustache.render(template, {episodes: data})
	$("#video_list_container").html(html)
	$("#videos_list").listview({
		autodividers: true,
		autodividersSelector: (li) ->
			season = $(li).data("season")
			return "Season #{season}"
	})

	$.mobile.loading("hide")

	return



	
	

