window.load_profile_details_page = (profile_id) ->
	
	url = "http://globalventures.herokuapp.com/profile/get_profile_by_id?profile_id=#{profile_id}"

	$.mobile.loading("show")

	$.getJSON url, 
		(data) ->
			profile_details_template = $("#profile_details_script").html()
			profile_details_html = Mustache.render profile_details_template, {profile: data}
			$("#profile_details_container").html(profile_details_html)
			$("#profile_detail_header_text").html(data.name)

			$.mobile.loading("hide")


			return


window.load_profile_photos_page = (profile_id) ->

	url = "http://globalventures.herokuapp.com/profile/get_profile_photos?profile_id=#{profile_id}"

	$.mobile.loading("show")

	$.ajax url,
		async: false,
		success: (data) ->		
			profile_photos_list_template = $("#photos_list_script").html()
			profile_photos_html = Mustache.render profile_photos_list_template, {photos: data}
			$("#profile_photos_list_container").html(profile_photos_html)	

			$.mobile.loading("hide")		

			return

	photoswipe = $("#photos_list a").photoSwipe({ jqueryMobile: true });

	return


