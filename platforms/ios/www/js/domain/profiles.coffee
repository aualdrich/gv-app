window.load_profiles_page = () ->	

	$.mobile.loading("show")	
	
	profiles = localStorage.getItem("profiles")
	data = JSON.parse(profiles)

	
	profiles_list_template = $("#profiles_list_script").html()
	profiles_list_html = Mustache.render profiles_list_template, {profiles: data}
	$("#profiles_list_container").html profiles_list_html
	$("#profiles_list").listview()

	$.mobile.loading("hide")

	return		
