window.load_episodes_page_detail = (episode_id) ->
	url = "http://globalventures.herokuapp.com/episode/get_episode_by_id?episode_id=#{episode_id}"

	$.mobile.loading("show")

	$.getJSON url,
			(data) ->

				air_date = moment.utc(data.air_date)
				air_date.local()				

				$("#episode_details_header_text").text(data.name)
				template = $("#episode_detail_script").html()
				html = Mustache.render(template, {main_photo_path_detail: data.main_photo_path_detail, description: data.description})
				$("#episode_detail_container").html(html)


				if(should_display_countdown(air_date))
					play_button_template = $("#episode_countdown_button_script").html()
					play_button_html = Mustache.render(play_button_template, {air_date: air_date.format("LLL")})
				else
					play_button_template = $("#play_episode_button_script").html()					
					play_button_html = Mustache.render(play_button_template, {episode_id: data.id})

				$("#play_button_container").html(play_button_html)
				$("#play_episode_link").on("click", () -> play_episode(data.link))

				$.mobile.loading("hide")


				return
		return


should_display_countdown = (air_date) ->
	return air_date.isAfter(moment())

play_episode = (episode_link) ->
	window.open episode_link, "_blank"
	return

